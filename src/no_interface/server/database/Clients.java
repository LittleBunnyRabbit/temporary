package database;

import java.net.Socket;

public class Clients {
    private Socket socket;
    private String name;
    private String loggInTime;
    private String color;

    public Clients(String name, Socket socket, String loggInTime, String color) {
        this.name = name;
        this.socket = socket;
        this.loggInTime = loggInTime;
        this.color = color;
    }

    public Socket getSocket() { return this.socket; }
    public String getName() { return this.name; }
    public String getLoggInTime() { return this.loggInTime; }
    public String getColor() { return this.color; }
}
