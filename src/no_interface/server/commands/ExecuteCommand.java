package commands;

import server.*;
import colors.*;
import file_transfer.*;
import database.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class ExecuteCommand {
    private Server server;
    private Socket clientSocket;
    private ColorList cl;

    public ExecuteCommand(Server server, Socket clientSocket, ColorList cl) {
        this.server = server;
        this.clientSocket = clientSocket;
        this.cl = cl;
    }

    public void getCommand(String data) {
        String[] split = splitInput(data);
        String command = split[0];
        data = split[1];
        runCommand(command.toUpperCase(), data);
    }

    public void runCommand(String command, String data) {
        switch (command) {
            case "DM":
                command_DM(data);
                break;

            case "EDIT":
                command_EDIT(data);
                break;

            case "HELP":
                command_HELP();
                break;

            case "SF":
                command_SENDFILE(data);
                break;

            case "GF":
                command_GETFILE(data);
                break;

            case "LF":
                command_LISTFILES();
                break;
        }
    }

    public String[] splitInput(String data) {
        String firstWord = "";
        int dataLength = data.length();
        for (int i = 0; i < dataLength; i++) {
            char c = data.charAt(i);
            if(c == ' ') {
                data = data.substring(i + 1);
                break;
            } else {
                firstWord = firstWord + c;
            }
        }
        return new String[]{firstWord, data};
    }

    public void command_HELP() {
        String br = "----------------------------------------";
        String intro = "Help list: ";
        String dm = "- DM: /dm {user} {text}";
        String edit = "- EDIT: /edit {color_code} {text}\n" + 
                      "  Example: /edit black:b:br \n" +
                      "  Codes: :b, :u, :bg, :br, :b:br, :bg:br "; 
        String msg = String.format("%s\n%s\n%s\n%s\n%s", br, intro, dm, edit, br);
        msg = String.format("%s%s%s", ConsoleColors.YELLOW, msg, ConsoleColors.RESET);
        server.sendToClient(clientSocket, msg);
    }

    public void command_DM(String data) {
        String[] split = splitInput(data);
        String user = split[0];
        data = split[1];

        String msg = null;

        for(Map.Entry<Socket, Clients> entry : server.clientList.entrySet()) {
            String clientName = entry.getValue().getName();
            if(clientName.equals(user)) {
                msg = ConsoleColors.YELLOW + "[" + server.clientList.get(clientSocket).getName() + " -> " + user + "]: " + ConsoleColors.RESET + data;
                server.serverMsg(msg);
                server.sendToClient(server.findClientSocket(user), msg);
                continue;
            }
        }
        if(msg == null) {
            server.sendToClient(clientSocket, "[server] Wrong username!");
        }
    }

    public void command_EDIT(String data) {
        String[] split = splitInput(data);
        String edit = split[0];
        data = split[1];
        if(cl.getList().containsKey(edit.toUpperCase())) {
            String color = cl.getList().get(edit.toUpperCase());
            String editedMsg = String.format("%s%s%s", color, data, ConsoleColors.RESET);
            server.sendToAllClients(clientSocket, editedMsg);
        }else {
            server.sendToClient(clientSocket, ConsoleColors.RED + "[server]: That edit doesnt exist!" + ConsoleColors.RESET);
        }
    }

    public void command_SENDFILE(String fileName) {
        if(!fileName.equals("") && !fileName.equals(" ")) {
            server.sendToClient(clientSocket, "[server] Transfering...");
            server.sendToClient(clientSocket, "$transfer" + " " + fileName);

            File dir = new File("./storage/");
            File file = new File(dir, fileName + ".enc");
            String data = "1";
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                while (!data.equals("$done")) {
                    data = server.checkForInput(server.createInputStream(clientSocket), clientSocket);
                    if(!data.equals("$done")) {
                        writer.write(data);
                        writer.newLine();
                    } else {
                        break;
                    }
                }

                writer.close();

            } catch (IOException e) {
                server.serverError("Unable to write file");
                server.sendToClient(clientSocket, "[server] Unable to write file");
            }

            server.sendToClient(clientSocket, "[server] File Transfered!");
        } else {
            server.sendToClient(clientSocket, "[server] Filename needed!");
        }
    }

    public void command_GETFILE(String fileName) {
        server.sendToClient(clientSocket, "[server] Transfering...");
        
        File folder = new File("./storage");
        File[] listOfFiles = folder.listFiles();
        String files = "";
        Boolean fileExists = false;

        for (File f : listOfFiles) {
            String name = f.getName();
            name = name.substring(0, name.length() - 4);
            if(name.equals(fileName)) {
                fileExists = true;
                break;
            }
        }

        if(fileExists) {

            server.sendToClient(clientSocket, "$download" + " " + fileName);

            try {
                File dir = new File("./storage/");
                File file = new File(dir, fileName + ".enc");
                BufferedReader br = new BufferedReader(new FileReader(file));
                String str;
                while((str = br.readLine()) != null) {
                    server.sendToClient(clientSocket, str); 
                }
                server.sendToClient(clientSocket, "$done"); 
            } catch (IOException e) {
                server.serverError("Unable to read file");
                server.sendToClient(clientSocket, "[server] Unable to read file");
            }
        } else {
            server.sendToClient(clientSocket, "[server] that file doesnt exist!");
        }

        server.sendToClient(clientSocket, "[server] File Transfered!");

    }   

    public void command_LISTFILES() {
        File folder = new File("./storage");
        File[] listOfFiles = folder.listFiles();
        String files = "";
        for (File f : listOfFiles) {
            String fileName = f.getName();
            fileName = fileName.substring(0, fileName.length() - 4);
            if(files.equals("")) {
                files = fileName;
            } else {
                files = files + ", " + fileName;
            }
        }
        server.sendToClient(clientSocket, ConsoleColors.YELLOW + files + ConsoleColors.RESET);
    }
}
