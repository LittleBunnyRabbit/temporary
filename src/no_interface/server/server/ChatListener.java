package server;

import commands.*;

import java.io.*;
import java.net.*;

public class ChatListener extends Thread {
    private Server server;
    private Socket clientSocket;
    private ExecuteCommand exc;

    public ChatListener(Server server, Socket clientSocket, ExecuteCommand exc) {
        this.server = server;
        this.clientSocket = clientSocket;
        this.exc = exc;
    }

    public void run() {
        server.serverMsg("Client connected! " + this.clientSocket.getPort());
        DataInputStream input = server.createInputStream(this.clientSocket);
        
        while(true) {
            String dataRecived = server.checkForInput(input, clientSocket);

            if(input == null || dataRecived == null) {
                break;
            } else if (dataRecived.length() == 0) {
                continue;
            }

            if(dataRecived.charAt(0) == '/') {
                exc.getCommand(dataRecived.substring(1));
                continue;
            }

            server.sendToAllClients(clientSocket, dataRecived);
        }
    }
}
