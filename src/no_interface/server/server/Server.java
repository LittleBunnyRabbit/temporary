package server;

import database.*;
import file_transfer.*;
import commands.*;
import colors.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.text.*;


public class Server {
    public final int SERVER_PORT = 1234;
    public final String SERVER_PASSWORD = "1111";
    public final String SERVER_CONSOLE_COLOR = ConsoleColors.WHITE;

    private ColorList cl = new ColorList();

    public HashMap<Socket, Clients> clientList = new HashMap<>();

    public static void main(String[] args) { new Server(); }

    public Server() {
        clearTerminal();
        serverLoggIn();
        createServer();
    }
    
    public void clearTerminal() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("clear");
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line=reader.readLine())!=null) {
                System.out.print(line);
            }
        } catch (Exception e) {

        } finally {
            process.destroy();
        }
    }

    public void serverMsg(String msg) { System.out.println(SERVER_CONSOLE_COLOR + "[server] " + msg + ConsoleColors.RESET); }
    public void serverError(String msg) { System.out.println(ConsoleColors.RED + "[error] " + msg + ConsoleColors.RESET); }

    public void serverLoggIn() {
        Scanner sc = new Scanner(System.in);
        String password = "";
        while(!password.equals(SERVER_PASSWORD)) {
            System.out.print("Password:");
            password = sc.next();

            if(!password.equals(SERVER_PASSWORD)) {
                System.out.println("[error] Wrong Password");
            }
        }
        sc.close();
    }

    public void createServer() {
        serverMsg("Creating server");

        ServerSocket serverSocket = null;

        try {
			serverSocket = new ServerSocket(this.SERVER_PORT);
		} catch (Exception e) {
            serverError("Couldnt create server socket on port: " + this.SERVER_PORT + "\nShutting down...");
			System.exit(1);
        }
        
        listenForClient(serverSocket);

        try {
			serverSocket.close();
		} catch (IOException e) {
			serverError("Couldnt close server socket\nShutting down...");
			System.exit(1);
		}

    }

    public void listenForClient(ServerSocket serverSocket) {
        serverMsg("Listening for a client...");
		try {
			while (true) {
				Socket newClientSocket = serverSocket.accept(); 
				synchronized(this) {
					setupClient(newClientSocket);
				}
				ChatListener connection = new ChatListener(this, newClientSocket, new ExecuteCommand(this, newClientSocket, cl));
				connection.start();
			}
		} catch (Exception e) {
			serverError("Couldnt connect to the client");
			System.exit(1);
		}
    }

    public void setupClient(Socket clientSocket) {
        DataInputStream input = createInputStream(clientSocket);
        String name = null;
        String password = null;

        while(!verifyUser(name, password) || name == null || password == null) {
            name = requestFromClient(clientSocket, input, "Username");
            password = requestFromClient(clientSocket, input, "Password");
        }

        String color = requestFromClient(clientSocket, input, "Color").toUpperCase();
        color = getColorCode(color);

        DateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy hh:mm:ss");
        Date date = new Date();
        String loggInTime = dateFormat.format(date).toString();

        clientList.put(clientSocket, new Clients(name, clientSocket, loggInTime, color));

        serverMsg(ConsoleColors.YELLOW +
                  "\nAdded client:" + 
                  "\n> Username: " + name +
                  "\n> Socket: " + clientSocket + 
                  "\n> Join Time: " + loggInTime + ConsoleColors.RESET);
                
        greetingsScreen(clientSocket);
    }

    public String getColorCode(String color) {
        switch (color) {
            case "BLACK":
                color = ConsoleColors.BLACK;
                break;
            case "RED":
                color = ConsoleColors.RED;
                break;
            case "GREEN":
                color = ConsoleColors.GREEN;
                break;
            case "YELLOW":
                color = ConsoleColors.YELLOW;
                break;
            case "BLUE":
                color = ConsoleColors.BLUE;
                break;
            case "PURPLE":
                color = ConsoleColors.PURPLE;
                break;
            case "CYAN":
                color = ConsoleColors.CYAN;
                break;
            case "WHITE":
                color = ConsoleColors.WHITE;
                break;

            default:
                color = ConsoleColors.WHITE;
                break;
        }
        return color;
    }

    public DataInputStream createInputStream(Socket clientSocket) {
        try {
			return new DataInputStream(clientSocket.getInputStream()); // create input stream for listening for incoming messages
		} catch (IOException e) {
            serverError("Could not open input stream!");
            return null;
        }
    }

    public String checkForInput(DataInputStream input, Socket clientSocket) {
        try {
            return input.readUTF();
        } catch (Exception e) {
            serverError("Problem when reading a msg");
            removeClient(clientSocket);
            return null;
        }
    }

    public String requestFromClient(Socket clientSocket, DataInputStream input, String req) {
        sendToClient(clientSocket, "[server] " + req + ":");
        String data = null;
        data = checkForInput(input, clientSocket);
        return data;
    }

    // TODO:
    public boolean verifyUser(String name, String password) {
        return true;
    }

    public void sendToClient(Socket clientSocket, String msg) {
        if(clientSocket != null) {
            try {
                DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream()); // create output stream for sending messages to the client
                output.writeUTF(msg);
            } catch (Exception e) {
                serverError("Couldnt send msg to client... Client: " + clientSocket.getPort());
            }
        }else {

        }
    }

    public void sendToAllClients(Socket clientSocket, String msg) {
        serverMsg(modifyUsername(clientSocket) + msg);
        for(Map.Entry<Socket, Clients> entry : clientList.entrySet()) {
            Socket socket = entry.getKey();
            if(socket != clientSocket) {
                sendToClient(socket, modifyUsername(clientSocket) + msg);
            }
        }
    }

    public String modifyUsername(Socket clientSocket) {
        String name = clientList.get(clientSocket).getName();
        String color = clientList.get(clientSocket).getColor();
        return color + "[" + name + "]: " + ConsoleColors.RESET;
    }

    public void greetingsScreen(Socket clientSocket) {
		sendToClient(clientSocket, "----------------------------------------");
		sendToClient(clientSocket, "Wellcome to the server " + clientList.get(clientSocket).getName() + "!\nOnline:");
            String players = "";
            
            for(Map.Entry<Socket, Clients> entry : clientList.entrySet()) {
                String color = entry.getValue().getColor();
                String name = color + entry.getValue().getName() + ConsoleColors.RESET;

                if(players.equals("")) {
					players = name;
				}else {
					players = players + ", " + name;
				}
            }
			
		sendToClient(clientSocket, players);
		sendToClient(clientSocket, "----------------------------------------");
    }

    public void removeClient(Socket clientSocket) {
        try {
            clientSocket.close();
        } catch (Exception e) {
            serverError("Could not close client socket");
        }
        sendToAllClients(clientSocket, ConsoleColors.RED_BACKGROUND + " Left the server..." + ConsoleColors.RESET);
        //serverMsg(clientList.get(clientSocket).getName() + " Left the server...");
        clientList.remove(clientSocket);
    }

    public Socket findClientSocket(String username) {
        for(Map.Entry<Socket, Clients> entry : clientList.entrySet()) {
            String clientName = entry.getValue().getName();
            if(clientName.equals(username)) {
                return entry.getKey();
            }
        }
        return null;
    }
}

