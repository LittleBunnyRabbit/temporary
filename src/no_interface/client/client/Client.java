package client;

import file_transfer.*;

import java.io.*;
import java.net.*;


public class Client extends Thread {
    private final int SERVER_PORT = 1234;
    protected static String serverIPV4;

    private ChatReceiver message_receiver;
    private DataOutputStream out;

    public static void main(String[] args) {
        serverIPV4 = args[0];
        try {
            new Client();
        } catch (Exception e) {
            System.out.println("\033[0;31m" + "[error]: " + "Could not create new Client");
            System.exit(1);
        }
    }

    public Client() throws Exception {
        clearTerminal();
        Socket socket = createSocketConnection();
		DataInputStream input = createInputStream(socket);
        DataOutputStream output = createOutputStream(socket);
        out = output;
        createChatReceiver(input, output);
        BufferedReader reader = createChatSender(output, input);

    }

    public void clearTerminal() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("clear");
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line=reader.readLine())!=null) {
                System.out.print(line);
            }
        } catch (Exception e) {

        } finally {
            process.destroy();
        }
    }

    public void clientMsg(String msg) { System.out.println("\033[0;31m" + "[client] " + msg + "\033[0m"); }

    public Socket createSocketConnection() {
        try {
            return new Socket(serverIPV4, SERVER_PORT);
        } catch (Exception e) {
            clientMsg("Could not create socket connection...");
            System.exit(1);
        }
        return null;
    }

    public DataInputStream createInputStream(Socket socket) {
        try {
            return new DataInputStream(socket.getInputStream());
        } catch (Exception e) {
            clientMsg("Could not create input stream...");
            System.exit(1);
        }
        return null;
    }

    public DataOutputStream createOutputStream(Socket socket) {
        try {
            return new DataOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            clientMsg("Could not create output stream...");
            System.exit(1);
        }
        return null;
    }

    public void createChatReceiver(DataInputStream input, DataOutputStream output) {
        try {
            message_receiver = new ChatReceiver(input, this, output);
            message_receiver.start();
        } catch (Exception e) {
            clientMsg("Could not create ChatReceiver");
            System.exit(1);
        }
    }

    public BufferedReader createChatSender(DataOutputStream output, DataInputStream input) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            String userInput;
            while ((userInput = reader.readLine()) != null) {
                sendMessage(userInput);
            }
        } catch (Exception e) {
            clientMsg("Could not create ChatSender");
            System.exit(1);
        }
        return reader;
    }

    public void sendMessage(String message) {
		try {
            out.writeUTF(message);
            out.flush();

		} catch (IOException e) {
            clientMsg("Could not send message...");
		}
    }
    
    private void closeClient(Socket socket, DataInputStream input, DataOutputStream output, BufferedReader reader) {
        try {
            output.close();
            input.close();
            reader.close();
            socket.close(); 
        } catch (Exception e) {
            clientMsg("Could not close client");
            System.exit(1);
        }
    }

    public String checkForInput(DataInputStream input) {
        try {
            return input.readUTF();
        } catch (Exception e) {
            System.out.println("ERROR READING THE FILE");
            System.exit(1);
            return null;
        }
    }

    public void sendFile(String fileName, DataOutputStream output) {
        EncoderDecoder.encode(fileName, this);

        try {
            File dir = new File("./sending/");
            File file = new File(dir, fileName + ".enc");

            BufferedReader br = new BufferedReader(new FileReader(file));
            String str;

            while((str = br.readLine()) != null) { 
                sendMessage(str);
            }

            sendMessage("$done");

        } catch (IOException e) {
            clientMsg("Could not read file");
        }

    }

    public void downloadFile(String fileName, DataInputStream input) {
        File dir = new File("./receiving/");
        File file = new File(dir, fileName + ".enc");

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            String encodedData = "1";
            while(!encodedData.equals("$done")) {
                encodedData = checkForInput(input);
                if(!encodedData.equals("$done")) {
                    try {
                        writer.write(encodedData);
                        writer.newLine();
                    } catch (IOException e) {
                        clientMsg("Unable to write line");
                    }

                } else {
                    writer.close();
                    break;
                }
            }

        } catch (IOException e) {
            clientMsg("Could not write files");
        }

        EncoderDecoder.decode(fileName, this);
    }

    public String[] splitInput(String data) {
        String firstWord = "";
        int dataLength = data.length();
        for (int i = 0; i < dataLength; i++) {
            char c = data.charAt(i);
            if(c == ' ') {
                data = data.substring(i + 1);
                break;
            } else {
                firstWord = firstWord + c;
            }
        }
        return new String[]{firstWord, data};
    }

}
