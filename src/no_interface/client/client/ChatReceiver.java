package client;

import file_transfer.*;

import java.io.*;
import java.net.*;

public class ChatReceiver extends Thread {	
	private DataInputStream input;
	private DataOutputStream output;
	private Client client;

	public ChatReceiver(DataInputStream input, Client client, DataOutputStream output) {
		this.input = input;
		this.client = client;
		this.output = output;
	}

	public void run() {
        while(true) {
            String dataRecived = client.checkForInput(input);

            if(input == null || dataRecived == null) {
                break;
            } else if (dataRecived.length() == 0) {
                continue;
            }

			if(dataRecived.charAt(0) == '$') {
				String[] data = client.splitInput(dataRecived.substring(1));
				switch (data[0]) {
					case "transfer":
						client.sendFile(data[1], output);
						break;

					case "download":
						client.downloadFile(data[1], input);
						break;

					default:
						System.out.println(dataRecived);
						break;
				}
				
			} else {
				System.out.println(dataRecived);
			}
        }
	}
}
