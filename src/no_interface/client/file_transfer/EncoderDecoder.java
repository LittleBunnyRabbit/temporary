package file_transfer;
import client.*;

import java.io.*;
import java.util.*;

public class EncoderDecoder {
    public static void encode(String fileName, Client client) {
        File dir = new File("./sending/");
        File file = new File(dir, fileName);
        String fileWDir = file.toPath().toString();

        try(FileInputStream fileInput = new FileInputStream(fileWDir)) {
            client.clientMsg("Started Encoding...");
            Base64.Encoder encoder = Base64.getMimeEncoder();
            OutputStream outputStream = encoder.wrap(new FileOutputStream(fileWDir + ".enc"));
            int _byte;
            while((_byte = fileInput.read()) != -1) {
                outputStream.write(_byte);
            }
            outputStream.close();
            client.clientMsg("Finished Encoding!");

        } catch (IOException ioe) {
            client.clientMsg("Failed to encode!");
            System.err.printf("I/O error: %s%n", ioe.getMessage());
        }
    }

    public static void decode(String fileName, Client client) {
        client.clientMsg("Started Decoding...");

        File dir = new File("./receiving/");
        File file = new File(dir, fileName);
        String fileWDir = file.toPath().toString();
        try(FileOutputStream fileOutput = new FileOutputStream(fileWDir)) {
            Base64.Decoder decoder = Base64.getMimeDecoder();
            InputStream inputStream = decoder.wrap(new FileInputStream(fileWDir + ".enc"));
            int _byte;
            while ((_byte = inputStream.read()) != -1) {
                fileOutput.write(_byte);
            }
            inputStream.close();

        } catch (IOException ioe) {
            System.err.printf("I/O error: %s%n", ioe.getMessage());
        }
        client.clientMsg("Finished Decoding!");
    }  
}